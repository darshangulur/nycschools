//
//  SchoolsDataModel.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import Foundation
import Combine

/// Data Model layer for School List API.
/// This layer will be used to fetch data from remote APIs, local database and caches.
final class SchoolsListDataModel {
    private var cancellables = Set<AnyCancellable>()
    private let apiClient: APIClientable
    
    init(apiClient: APIClientable) {
        self.apiClient = apiClient
    }
}

protocol SchoolsListable {
    func fetchSchools(offset: Int) -> AnyPublisher<[School], APIError>
}

// MARK: SchoolsListable conformance
extension SchoolsListDataModel: SchoolsListable {
    func fetchSchools(offset: Int) -> AnyPublisher<[School], APIError> {
        let publisher = PassthroughSubject<[School], APIError>()
        
        apiClient
            .fetchData(
                routable: API.Schools.getSchools(
                    offset: offset
                )
            )
            .sink { completion in
                if case let .failure(error) = completion {
                    publisher.send(completion: .failure(error))
                }
            } receiveValue: { (schools: [School]) in
                publisher.send(schools)
            }
            .store(in: &cancellables)
        return publisher.eraseToAnyPublisher()
    }
}
