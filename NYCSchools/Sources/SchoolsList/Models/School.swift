//
//  School.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import Foundation

struct School: Codable {
    let id: String
    let name: String
    let boro: String
    var borough: String?
    let overview: String
    let city: String
    let state: String
    let phone: String
    let email: String?
    let website: String
    var latitude: String?
    var longitude: String?
    
    private enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case boro
        case borough
        case overview = "overview_paragraph"
        case city
        case state = "state_code"
        case phone = "phone_number"
        case email = "school_email"
        case website
        case latitude
        case longitude
    }
}

extension School: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: School, rhs: School) -> Bool {
        lhs.id == rhs.id
    }
}
