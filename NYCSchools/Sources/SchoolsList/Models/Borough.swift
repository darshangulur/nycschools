//
//  Borough.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/8/22.
//

import Foundation

struct Borough: Identifiable, Hashable, Equatable {
    let id: Int
    let name: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func ==(lhs: Borough, rhs: Borough) -> Bool {
        lhs.id == rhs.id
    }
}
