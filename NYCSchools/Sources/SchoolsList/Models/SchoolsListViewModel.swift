//
//  SchoolsListViewModel.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/3/22.
//

import Foundation
import Combine

protocol SchoolsListViewModelable: ObservableObject {
    var schoolsToDisplay: [School] { get }
    var error: String? { get }
    var boroughs: [Borough] { get }
    var selectedBorough: Int { get set }
    var searchText: String { get set }
    var isFetching: Bool { get }
    var shouldAllowInfiniteScrolling: Bool { get }
    
    static var defaultBorough: Borough { get }
    
    func fetchSchools()
    func filterSchools(searchText: String, boroughChoice: Borough)
}

final class SchoolsListViewModel: SchoolsListViewModelable {
    @Published var schoolsToDisplay = [School]()
    @Published var error: String?
    @Published var boroughs = [Borough]()
    @Published var selectedBorough: Int = defaultBorough.id
    @Published var searchText: String = ""
    @Published var isFetching: Bool = false
    
    private(set) static var defaultBorough: Borough = Borough(id: 0, name: "All Boroughs")
    
    private var allSchools = [School]()
    private var didReachEndOfList: Bool = false
    private var cancellables = Set<AnyCancellable>()
    
    private let dataModel: SchoolsListable
    init(dataModel: SchoolsListable) {
        self.dataModel = dataModel
        
        Publishers.CombineLatest($selectedBorough, $searchText)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (selectedBorough, searchText) in
                guard let self = self,
                      !self.boroughs.isEmpty else {
                    return
                }
                
                self.filterSchools(
                    searchText: searchText,
                    boroughChoice: self.boroughs[selectedBorough]
                )
            }
            .store(in: &cancellables)
    }
    
    var shouldAllowInfiniteScrolling: Bool {
        selectedBorough == Self.defaultBorough.id && searchText.isEmpty
    }
    
    func fetchSchools() {
        isFetching = true
        
        if !didReachEndOfList {
            dataModel.fetchSchools(offset: allSchools.count)
                .receive(on: DispatchQueue.main)
                .sink { [weak self] completion in
                    guard let self = self,
                          case let .failure(error) = completion else {
                        return
                    }
                    
                    Logger.debug("Error fetching Schools: \(error)")
                    self.error = "Unable to fetch schools at this time."
                    self.allSchools = []
                    self.schoolsToDisplay = []
                    self.didReachEndOfList = false
                    self.isFetching = false
                } receiveValue: { [weak self] schools in
                    guard let self = self else {
                        self?.isFetching = false
                        return
                    }
                    
                    Logger.debug("Fetched schools: \(schools.count)")                    
                    self.allSchools.append(contentsOf: schools)
                    self.schoolsToDisplay.append(contentsOf: schools)
                    self.error = nil
                    
                    // Prepare boroughs list for filtering based on the available list of schools
                    let boroughNames = Set<String>(self.allSchools.compactMap { $0.borough })
                    var boroughs: [Borough] = [Self.defaultBorough]
                    boroughNames.forEach {
                        boroughs.append(
                            Borough(
                                id: boroughs.count,
                                name: $0.trimmingCharacters(in: .whitespaces).capitalized
                            )
                        )
                    }
                    self.boroughs = boroughs
                    
                    // Check if the result is empty and if yes do not perform infinite scroll refresh
                    if schools.isEmpty == true {
                        Logger.debug("Reached end of list")
                        self.didReachEndOfList = true
                    }
                    
                    self.isFetching = false
                }
                .store(in: &cancellables)
        } else {
            isFetching = false
        }
    }
    
    /// Filter schools based on search term matching a part of school name
    func filterSchools(searchText: String, boroughChoice: Borough) {
        var filteredSchools = allSchools
        
        if !searchText.isEmpty {
            let namePredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
            filteredSchools = filteredSchools.filter {
                namePredicate.evaluate(with: $0.name)
            }
        }
        
        if boroughChoice != Self.defaultBorough {
            let boroughPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", boroughChoice.name)
            filteredSchools = filteredSchools.filter {
                boroughPredicate.evaluate(with: $0.borough)
            }
        }
        
        schoolsToDisplay = filteredSchools
    }
}
