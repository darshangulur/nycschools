//
//  ContentView.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import SwiftUI

struct SchoolsListView<ViewModel: SchoolsListViewModelable>: View {
    struct Constants {
        let title: String = "Schools"
        let searchPlaceholderText = "Search schools"
        let boroughSelectionTitle: String = "Select"
        let cancelButtonTitle: String = "Cancel"
        
        let overviewLineCount: Int = 2
        let itemSpacing: CGFloat = 5.0
        let infinteScrollTrigger: Int = 10
        let gray: Color = Color(red: 224/255, green: 224/255, blue: 224/255)
        let animationDuration: Double = 0.2
        let inputViewVerticalPadding: CGFloat = 0.2
        
        let searchGlassIconName: String = "magnifyingglass"
        let clearIconName: String = "xmark.circle.fill"
    }
    
    @ObservedObject var viewModel: ViewModel
    
    @FocusState var isFocussed: Bool
    @State var shouldShowCancelButton: Bool = false
    
    private let constants = Constants()

    var body: some View {
        NavigationView {
            content
                .navigationTitle(constants.title)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(
                        placement: .navigationBarTrailing
                    ) {
                        boroughSelection
                    }
                }
                .onAppear {
                    viewModel.fetchSchools()
                }
        }
    }

    var content: some View {
        VStack(spacing: 0.0) {
            inputView
            
            ZStack {
                if !viewModel.schoolsToDisplay.isEmpty {
                    schoolsList
                } else if let error = viewModel.error {
                    Text(error)
                }
                
                if viewModel.isFetching {
                    loadingView
                }
            }
            
            Spacer() // to make sure the content is always pinned to the top
        }
    }

    var inputView: some View {
        VStack {
            HStack {
                Image(systemName: constants.searchGlassIconName)
                TextField(
                    constants.searchPlaceholderText,
                    text: $viewModel.searchText
                ){ isEditing in
                    withAnimation(
                        .easeIn(duration: constants.animationDuration)
                    ) {
                        shouldShowCancelButton = isEditing
                    }
                }
                .focused($isFocussed)
                
                if !viewModel.searchText.isEmpty {
                    Button {
                        viewModel.searchText = ""
                    } label: {
                        Image(systemName: constants.clearIconName)
                            .foregroundColor(.gray)
                    }
                }
                
                if shouldShowCancelButton {
                    Button {
                        viewModel.searchText = ""
                        isFocussed.toggle()
                    } label: {
                        Text(constants.cancelButtonTitle)
                    }
                }
            }
            .padding(.horizontal)
            .padding(.vertical, constants.inputViewVerticalPadding)
            
            Divider()
        }
    }

    var schoolsList: some View {
        List(viewModel.schoolsToDisplay, id: \.id) { school in
            NavigationLink(
                destination: {
                    SchoolDetailView(
                        viewModel: SchoolDetailsViewModel(
                            school: school,
                            dataModel: SchoolDetailsDataModel(
                                apiClient: APIClient()
                            )
                        )
                    )
                }, label: {
                    makeSchoolRow(school)
                }
            )
        }
        .listStyle(.plain)
    }

    func makeSchoolRow(_ school: School) -> some View {
        VStack(
            alignment: .leading,
            spacing: constants.itemSpacing
        ) {
            Text(school.name)
                .font(.title3)
            
            if viewModel.selectedBorough == ViewModel.defaultBorough.id,
               let borough = school.borough {
                Text(borough)
                    .font(.callout)
                    .foregroundColor(Color.gray)
            }
            
            Text(school.overview)
                .font(.callout)
                .lineLimit(constants.overviewLineCount)
                .foregroundColor(Color.gray)
        }
        .onAppear {
            // Infinite scrolling
            guard viewModel.shouldAllowInfiniteScrolling else {
                return
            }

            let count = viewModel.schoolsToDisplay.count
            let trigger = constants.infinteScrollTrigger
            if count > trigger,
               school == viewModel.schoolsToDisplay[count - trigger] {
                viewModel.fetchSchools()
            }
        }
    }

    @ViewBuilder
    var boroughSelection: some View {
        if !viewModel.boroughs.isEmpty {
            Menu(viewModel.boroughs[viewModel.selectedBorough].name) {
                Picker("", selection: $viewModel.selectedBorough) {
                    ForEach(viewModel.boroughs, id: \.id) { borough in
                        Text("\(borough.name)")
                            .tag(borough.id)
                    }
                }
            }
            .animation(nil, value: UUID()) // stop unnecessary animation
        }
    }
    
    var loadingView: some View {
        ProgressView()
    }
}

#if DEBUG
struct SchoolsListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsListView(
            viewModel: SchoolsListViewModel(
                dataModel: SchoolsListDataModel(
                    apiClient: APIClient()
                )
            )
        )
    }
}
#endif
