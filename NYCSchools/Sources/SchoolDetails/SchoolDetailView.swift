//
//  SchoolDetailView.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/8/22.
//

import SwiftUI

struct SchoolDetailView<ViewModel: SchoolDetailsViewModleable>: View {
    struct Constants {
        let socialLinkItemVerticalSpacing: CGFloat = 10.0
        let sectionTitleBottomSpacing: CGFloat = 5.0
        
        let title: String = "School Details"
        let overviewSectionHeader: String = "Overview"
        let scoreSectionHeader: String = "SAT Scores (Average):"
        let contactSectionHeader: String = "Contact:"
        let math: String = "Math:"
        let reading: String = "Critical Reading:"
        let writing: String = "Writing:"
        let studentCount: String = "# Students:"
        let directionsText: String = "Directions"
        
        let websiteIconName: String = "globe"
        let emailIconName: String = "mail"
        let phoneIconName: String = "phone"
        let directionsIconName: String = "map.circle"
    }
    
    private let constants = Constants()
    @ObservedObject var viewModel: ViewModel

    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                schoolDetails
                
                scores
                
                makeSocialLinks(viewModel.school)
            }
            .padding(.horizontal)
        }
        .padding(.bottom)
        .onAppear {
            viewModel.fetchScore()
        }
        .navigationTitle(constants.title)
        .navigationBarTitleDisplayMode(.inline)
    }

    var schoolDetails: some View {
        VStack(alignment: .leading) {
            Text(viewModel.school.name)
                .font(.title2)
            
            Text("\(viewModel.school.city), \(viewModel.school.state)".uppercased())
                .font(.subheadline)
                .foregroundColor(.gray)
                .padding(.bottom)
            
            makeSectionHeader(text: constants.overviewSectionHeader)
            
            Text(viewModel.school.overview)
                .font(.body)
        }
    }

    @ViewBuilder
    var scores: some View {
        Group {
            VStack(alignment: .leading) {
                makeSectionHeader(text: constants.scoreSectionHeader)
                
                if let score = viewModel.score {
                    makeScoreRow(
                        subject: constants.math,
                        score: score.math
                    )
                    
                    makeScoreRow(
                        subject: constants.reading,
                        score: score.reading
                    )
                    
                    makeScoreRow(
                        subject: constants.writing,
                        score: score.writing
                    )
                    
                    makeScoreRow(
                        subject: constants.studentCount,
                        score: score.numberOfTestTakers
                    )
                } else if let error = viewModel.error {
                    Text(error)
                } else if viewModel.isFetching {
                    ProgressView()
                }
            }
        }
        .padding(.top)
    }

    func makeScoreRow(subject: String, score: String) -> some View {
        HStack {
            Text(subject)
            Text(score)
        }
    }

    func makeSocialLinks(_ school: School) -> some View {
        VStack(
            alignment: .leading,
            spacing: constants.socialLinkItemVerticalSpacing
        ) {
            makeSectionHeader(text: constants.contactSectionHeader)
            
            if let websiteURL = viewModel.websiteURL {
                makeSocialLinkRow(
                    imageName: constants.websiteIconName,
                    text: school.website,
                    destination: websiteURL
                )
            }
            
            if let email = school.email,
               let emailLink = viewModel.emailURL {
                makeSocialLinkRow(
                    imageName: constants.emailIconName,
                    text: email,
                    destination: emailLink
                )
            }

            if let phoneURL = viewModel.phoneURL {
                makeSocialLinkRow(
                    imageName: constants.phoneIconName,
                    text: school.phone,
                    destination: phoneURL
                )
            }
            
            if let directionsURL = viewModel.directionsURL {
                makeSocialLinkRow(
                    imageName: constants.directionsIconName,
                    text: constants.directionsText,
                    destination: directionsURL
                )
            }
        }
        .padding(.vertical)
    }
    
    func makeSocialLinkRow(
        imageName: String,
        text: String,
        destination: URL
    ) -> some View {
        HStack {
            Image(systemName: imageName)
            Link(
                text,
                destination: destination
            )
            .font(.body)
        }
    }

    func makeSectionHeader(text: String) -> some View {
        Text(text)
            .underline()
            .font(.title3)
            .padding(.bottom, constants.sectionTitleBottomSpacing)
    }
}

#if DEBUG
struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailView(
            viewModel: SchoolDetailsViewModel(
                school: Stub.school,
                dataModel: SchoolDetailsDataModel(
                    apiClient: APIClient()
                )
            )
        )
    }
}
#endif
