//
//  SchoolDetailsViewModel.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/8/22.
//

import Foundation
import Combine

protocol SchoolDetailsViewModleable: ObservableObject {
    var school: School { get }
    var score: Score? { get }
    var error: String? { get }
    var isFetching: Bool { get }
    
    var websiteURL: URL? { get }
    var emailURL: URL? { get }
    var phoneURL: URL? { get }
    var directionsURL: URL? { get }
    
    func fetchScore()
}

final class SchoolDetailsViewModel: SchoolDetailsViewModleable {
    @Published private(set) var school: School
    @Published var score: Score?
    @Published var error: String?
    @Published var isFetching: Bool = false
    
    private var cancellables = Set<AnyCancellable>()
    
    let dataModel: SchoolDetailable
    init(school: School, dataModel: SchoolDetailable) {
        self.school = school
        self.dataModel = dataModel
    }
    
    var websiteURL: URL? {
        if school.website.contains("https://") || school.website.contains("http://") {
            return URL(string: school.website)
        }
        
        return URL(string: "https://\(school.website)")
    }

    var emailURL: URL? {
        guard let email = school.email else {
            return nil
        }
        
        return URL(string: "mailto:\(email)")
    }
    
    var phoneURL: URL? {
        URL(string: "tel:\(school.phone)")
    }
    
    var directionsURL: URL? {
        guard let latitude = school.latitude, let longitude = school.longitude else {
            return nil
        }
        
        return URL(string: "maps://?saddr=&daddr=\(latitude),\(longitude)")
    }
    
    func fetchScore() {
        isFetching = true
        
        dataModel.fetchScore(schoolId: school.id)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                guard let self = self else {
                    self?.isFetching = false
                    return
                }
                
                if case .failure = completion {
                    Logger.debug("Score fetch failed")
                    self.error = "Scores not available."
                }
                
                self.isFetching = false
            } receiveValue: { [weak self] score in
                guard let self = self else {
                    self?.isFetching = false
                    return
                }
                
                Logger.debug("Fetched scores.")
                self.score = score
                self.isFetching = false
            }
            .store(in: &cancellables)
    }
}
