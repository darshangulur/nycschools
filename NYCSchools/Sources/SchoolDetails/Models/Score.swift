//
//  Score.swift
//  NYCSchools
//
//  Created by Darshan Gulur Srinivasa on 12/2/22.
//

struct Score: Codable {
    var reading: String
    var math: String
    var writing: String
    var numberOfTestTakers: String
    
    private enum CodingKeys: String, CodingKey {
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
        case numberOfTestTakers = "num_of_sat_test_takers"
    }
}
