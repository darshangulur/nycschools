//
//  SchoolsDataModel.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import Foundation
import Combine

/// Data Model layer for School Details API.
/// This layer will be used to fetch data from remote APIs, local database and caches.
final class SchoolDetailsDataModel {
    private var cancellables = Set<AnyCancellable>()
    private let apiClient: APIClientable
    
    init(apiClient: APIClientable) {
        self.apiClient = apiClient
    }
}

protocol SchoolDetailable {
    func fetchScore(schoolId: String) -> AnyPublisher<Score, APIError>
}

// MARK: SchoolDetailable conformance
extension SchoolDetailsDataModel: SchoolDetailable {
    func fetchScore(schoolId: String) -> AnyPublisher<Score, APIError> {
        let publisher = PassthroughSubject<Score, APIError>()
        apiClient
            .fetchData(
                routable: API.Schools.getScore(
                    schoolId: schoolId
                )
            )
            .sink { completion in
                if case let .failure(error) = completion {
                    publisher.send(completion: .failure(error))
                }
            } receiveValue: { (scores: [Score]) in
                guard let score = scores.first else {
                    publisher.send(completion: .failure(APIError.noData))
                    return
                }
                
                publisher.send(score)
            }
            .store(in: &cancellables)
        return publisher.eraseToAnyPublisher()
    }
}
