//
//  Stub.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/9/22.
//

import Foundation

#if DEBUG
enum Stub {
   static let school = School(
        id: "21K728",
        name: "Manhattan High School",
        boro: "Manhattan",
        overview: "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
        city: "Manhattan",
        state: "NY",
        phone: "226-348-7890",
        email: "manhattan@school.com",
        website: "https://www.manhattan.school"
    )
}
#endif
