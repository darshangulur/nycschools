//
//  Logger.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import Foundation

/// Debug Logger, also can be re-used to log to a remote service for Analytics.
enum Logger {
    static func debug(_ message: String, function: String = #function, line: Int = #line) {
        print("\(function), Line# \(line): \(message)")
    }
}
