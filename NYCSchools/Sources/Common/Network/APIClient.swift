//
//  APIClient.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import Foundation
import Combine

protocol APIClientable {
    func fetchData<T: Decodable>(routable: Routable) -> AnyPublisher<T, APIError>
}

/// Network layer that performs API calls, parses responses using given generic type and publishes data objects.
struct APIClient: APIClientable {
    func fetchData<T: Decodable>(routable: Routable) -> AnyPublisher<T, APIError> {
        guard let url = routable.url else {
            return Fail<T, APIError>(error: .invalidRequest).eraseToAnyPublisher()
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = routable.method.rawValue
        
        return URLSession.DataTaskPublisher(request: request, session: .shared)
            .map { $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { error in
                return APIError.parser
            }
            .eraseToAnyPublisher()
    }
}

enum APIError: Error {
    case invalidRequest
    case parser
    case generic
    case noData
}
