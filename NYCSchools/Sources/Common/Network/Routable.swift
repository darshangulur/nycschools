//
//  Routable.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

/// A data structure to encapsulate all the info needed to perform an API call.
protocol Routable {
    var path: String { get }
    var method: HTTPMethod { get }
    var queryParams: [String: String]? { get }
}

extension Routable {
    var queryParams: [String: String]? {
        return nil
    }
    
    var url: URL? {
        var urlComponents = URLComponents(string: AppConfig.shared.basePath + path)
        urlComponents?.queryItems = queryParams?.map {
            URLQueryItem(name: $0.key, value: $0.value)
        }
        return urlComponents?.url
    }
}
