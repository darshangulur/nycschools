//
//  AppConfig.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import Foundation

/// A data structure to encapsulate all app level constants.
struct AppConfig {
    static let shared = AppConfig()
    
    let basePath = "https://data.cityofnewyork.us/resource/"
}
