//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolsListView(
                viewModel: SchoolsListViewModel(
                    dataModel: SchoolsListDataModel(
                        apiClient: APIClient()
                    )
                )
            )
        }
    }
}
