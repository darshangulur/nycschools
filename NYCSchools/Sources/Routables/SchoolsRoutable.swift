//
//  SchoolsRoutable.swift
//  NYCSchools
//
//  Created by Darshan Srinivasa on 12/2/22.
//

import Foundation

enum API {
    enum Schools: Routable {
        case getSchools(offset: Int)
        case getScore(schoolId: String)
        
        var method: HTTPMethod {
            return .get
        }
        
        var path: String {
            switch self {
            case .getSchools:
                return "s3k6-pzi2.json"
            case .getScore:
                return "f9bf-2cp4.json"
            }
        }
        
        var queryParams: [String : String]? {
            switch self {
            case .getSchools(let offset):
                let params: [String: String] = [
                    "$limit": "\(20)",
                    "$offset": "\(offset)",
                    "$order": ":id"
                ]
                return params
            case .getScore(let schoolId):
                let params: [String: String] = [
                    "dbn": schoolId
                ]
                return params
            }
        }
    }
}
