//
//  SchoolDetailsViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Darshan Srinivasa on 12/9/22.
//

import XCTest
import Combine

@testable import NYCSchools

import OHHTTPStubs
import OHHTTPStubsSwift

final class SchoolDetailsViewModelTests: XCTestCase {
    var sut: SchoolDetailsViewModel!
    
    private var cancellables = Set<AnyCancellable>()
    private var school: School = School(
        id: "21K728",
        name: "NYC High School 1",
        boro: "A world class school.",
        borough: "Q",
        overview: "Queens",
        city: "New York City",
        state: "NY",
        phone: "213-548-1876",
        email: "nyc1@highschool.com",
        website: "www.nychighschool1.com",
        latitude: "22.452378",
        longitude: "-78.216587"
    )
    var dataModel: SchoolDetailsDataModel!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        dataModel = SchoolDetailsDataModel(apiClient: APIClient())
        sut = SchoolDetailsViewModel(
            school: school,
            dataModel: dataModel
        )
    }

    override func tearDownWithError() throws {
        sut = nil
        dataModel = nil
        try super.tearDownWithError()
    }
    
    func testContactURLs() {
        // test happy paths
        XCTAssertEqual(sut.websiteURL?.absoluteString, "https://www.nychighschool1.com")
        XCTAssertEqual(sut.emailURL?.absoluteString, "mailto:nyc1@highschool.com")
        XCTAssertEqual(sut.phoneURL?.absoluteString, "tel:213-548-1876")
        XCTAssertEqual(sut.directionsURL?.absoluteString, "maps://?saddr=&daddr=22.452378,-78.216587")
        
        // test error paths
        school = School(
            id: "21K728",
            name: "NYC High School 1",
            boro: "A world class school.",
            borough: "Q",
            overview: "Queens",
            city: "New York City",
            state: "NY",
            phone: "213-548-1876",
            email: nil,
            website: "https://www.nychighschool1.com",
            latitude: nil,
            longitude: nil
        )
        sut = SchoolDetailsViewModel(
            school: school,
            dataModel: dataModel
        )
        XCTAssertEqual(sut.websiteURL?.absoluteString, "https://www.nychighschool1.com")
        XCTAssertNil(sut.emailURL?.absoluteString)
        XCTAssertEqual(sut.phoneURL?.absoluteString, "tel:213-548-1876")
        XCTAssertNil(sut.directionsURL?.absoluteString)
    }

    func testFetchSchoolDetailsSuccess() {
        stub(condition: isHost("data.cityofnewyork.us")) { request in
          return HTTPStubsResponse(
            fileAtPath: OHPathForFile("score.json", type(of: self))!,
            statusCode: 200,
            headers: ["Content-Type":"application/json"]
          )
        }
        
        var shouldAllowToValidate = false
        let expectations = XCTestExpectation(description: "Score data fetched")
        sut.$score
            .sink { score in
                guard shouldAllowToValidate else {
                    return
                }
                
                XCTAssertEqual(score?.math, "327")
                expectations.fulfill()
            }
            .store(in: &cancellables)
        
        shouldAllowToValidate = true
        sut.fetchScore()
        wait(for: [expectations], timeout: 5.0)
    }

    func testFetchScoreEmpty() {
        stub(condition: isHost("data.cityofnewyork.us")) { request in
          return HTTPStubsResponse(
            fileAtPath: OHPathForFile("empty.json", type(of: self))!,
            statusCode: 200,
            headers: ["Content-Type":"application/json"]
            )
        }

        var shouldAllowToValidate = false
        let expectations = XCTestExpectation(description: "Score data is empty")
        sut.$error
            .sink { error in
                guard shouldAllowToValidate else {
                    return
                }
                
                XCTAssertNotNil(error)
                expectations.fulfill()
            }
            .store(in: &cancellables)
        
        shouldAllowToValidate = true
        sut.fetchScore()
        wait(for: [expectations], timeout: 5.0)
    }

    func testFetchSchoolDetailsfailure() {
        stub(condition: isHost("data.cityofnewyork.us")) { request in
          return HTTPStubsResponse(
            fileAtPath: OHPathForFile("error.json", type(of: self))!,
            statusCode: 400,
            headers: ["Content-Type":"application/json"]
          )
        }

        var shouldAllowToValidate = false
        let expectations = XCTestExpectation(description: "Score data not fetched")
        sut.$error
            .sink { error in
                guard shouldAllowToValidate else {
                    return
                }
                
                XCTAssertNotNil(error)
                expectations.fulfill()
            }
            .store(in: &cancellables)
        
        shouldAllowToValidate = true
        sut.fetchScore()
        wait(for: [expectations], timeout: 5.0)
    }
}
