//
//  SchoolsListViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Darshan Srinivasa on 12/9/22.
//

import XCTest
import Combine

@testable import NYCSchools

import OHHTTPStubs
import OHHTTPStubsSwift

final class SchoolsListViewModelTests: XCTestCase {
    var sut: SchoolsListViewModel!
    
    private var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        
        let dataModel = SchoolsListDataModel(apiClient: APIClient())
        sut = SchoolsListViewModel(dataModel: dataModel)
    }
    
    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func testFetchSchoolsListSuccess() {
        stub(condition: isHost("data.cityofnewyork.us")) { request in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("schools.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type":"application/json"]
            )
        }
        
        var shouldAllowToValidate = false
        let expectations = XCTestExpectation(description: "Schools list fetched")
        sut.$schoolsToDisplay
            .sink { schools in
                guard shouldAllowToValidate else {
                    return
                }
                
                XCTAssertEqual(schools.count, 20)
                expectations.fulfill()
            }
            .store(in: &cancellables)
        
        shouldAllowToValidate = true
        sut.fetchSchools()
        wait(for: [expectations], timeout: 5.0)
    }
    
    func testFetchSchoolsListEmpty() {
        stub(condition: isHost("data.cityofnewyork.us")) { request in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("empty.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type":"application/json"]
            )
        }
        
        var shouldAllowToValidate = false
        let expectations = XCTestExpectation(description: "Schools list is empty")
        sut.$schoolsToDisplay
            .sink { schools in
                guard shouldAllowToValidate else {
                    return
                }
                
                XCTAssertEqual(schools.count, 0)
                expectations.fulfill()
            }
            .store(in: &cancellables)
        
        shouldAllowToValidate = true
        sut.fetchSchools()
        wait(for: [expectations], timeout: 5.0)
    }
    
    func testFetchSchoolsListFailure() {
        stub(condition: isHost("data.cityofnewyork.us")) { request in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("error.json", type(of: self))!,
                statusCode: 400,
                headers: ["Content-Type":"application/json"]
            )
        }
        
        var shouldAllowToValidate = false
        let expectations = XCTestExpectation(description: "Schools list fetch fails")
        sut.$error
            .sink { error in
                guard shouldAllowToValidate else {
                    return
                }
                
                XCTAssertNotNil(error)
                expectations.fulfill()
            }
            .store(in: &cancellables)
        
        shouldAllowToValidate = true
        sut.fetchSchools()
        wait(for: [expectations], timeout: 5.0)
    }
    
    func testFilteredSchoolsListSuccess() {
        stub(condition: isHost("data.cityofnewyork.us")) { request in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("schools.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type":"application/json"]
            )
        }
        
        var shouldAllowToValidate = false
        var expectedResult = 20
        var expectations = XCTestExpectation(description: "Schools list fetched")
        sut.$schoolsToDisplay
            .sink { schools in
                guard shouldAllowToValidate else {
                    return
                }
                
                XCTAssertEqual(schools.count, expectedResult)
                expectations.fulfill()
            }
            .store(in: &cancellables)
        
        shouldAllowToValidate = true
        sut.fetchSchools()
        wait(for: [expectations], timeout: 5.0)
        
        // filter for keyword `High` with default borough
        expectedResult = 8
        expectations = XCTestExpectation(description: "Schools list filtered for keyword `High`")
        sut.filterSchools(searchText: "High", boroughChoice: SchoolsListViewModel.defaultBorough)
        wait(for: [expectations], timeout: 5.0)
        
        // filter for schools in Manhattan
        expectedResult = 4
        expectations = XCTestExpectation(description: "Schools list filtered for Manhattan")
        sut.filterSchools(searchText: "", boroughChoice: Borough(id: 1, name: "Manhattan"))
        wait(for: [expectations], timeout: 5.0)
        
        // filter for keyword `High` without calling filter function
        expectedResult = 8
        expectations = XCTestExpectation(description: "Schools list filtered for keyword `high`")
        sut.searchText = "High"
        wait(for: [expectations], timeout: 5.0)
    }
}
