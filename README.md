#  NYCSchools
- Follows MVVM design pattern.
- Adheres to SOLID design principles.
- Highly scalable and testable Network, Data Model and View Model layers.
- Unit Test coverage ~70%.
- Dependency management using SPM. External libraries used: OHHTTPStubs.

## v0.1.0 released on 2022/12/09
- Created Schools List screen.
    - Additional functionality: 
        - Search schools by title
        - Scroll infinitely until the end of the list. Known shortcoming: This feature is made to work only when list is not filtered either by boroughs or search keyword. 
        - Filter by borough names
    
- Created School Details screen presenting school overview and scores. 
    - Additional functionailty:
        - Ability to Interact with phone, email, web and directions links.
    
- Added unit tests for the network, data model and view model layers.

## Future work
 1. Add Horizontal list of `Chip` like buttons to filter content based on Burrows with a single tap.
 2. Expand search based on other `School` data model attributes.
 3. Replace OHTTPStubs with a custom MockAPIClient Implementation.
 4. Improvise code coverage.
 5. Handle No Network scenario. Also, probably cache data to show content when offline?
 6. Add snapshot tests. Write UI tests.
